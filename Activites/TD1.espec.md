Note: an easier and faster way to install R, Python, Rstudio and Jupyter may be to install
[Anaconda](https://www.anaconda.com/distribution/).

## TD1: Setup

1. Download and Install **git** and **RStudio** (take the free version)
   - Git: https://git-scm.com/downloads
     - _Understand git and Tutorial_: https://try.github.io/
   - RStudio: https://www.rstudio.com/products/rstudio/download/
     for mac users you need to download R https://cran.r-project.org/

2. Login to your Gitlab account
   - Go to the login page: https://gricad-gitlab.univ-grenoble-alpes.fr
   - Using UGA Agalan login/password
   - _Documentation_: Have a look at the Gitlab documentation https://docs.gitlab.com/ee/gitlab-basics/README.html

3. Add your public SSH key
   - Add it here: https://gricad-gitlab.univ-grenoble-alpes.fr/profile/keys
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

4. Fork the reference repository
   - Fork this repository: https://gricad-gitlab.univ-grenoble-alpes.fr/MSPL/mspl-2019-2020
     - Please, use the `Fork` button in the web interface
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/fork-project.html

5. Clone your forked repository locally
   - `git clone git@gricad-gitlab.univ-grenoble-alpes.fr:YOURLOGIN/mspl-2019-2020.git`
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html


6. Create your first commit, then push it to gitlab
   - Configure git:
```
git config --global user.name "MY NAME"
git config --global user.email "MY EMAIL"
```
   - Create a file `AUTHOR.txt` with your name
   - Then, run `git add AUTHOR.txt`
   - Commit it locally using `git commit -m "my first commit"`
   - Push to github with `git push origin master`

7. Add mspl-2019-2020 remote

   Do in your local repository, the one that you've cloned, the following commands:
   - Run `git remote add mspl-2019-2020 git@gricad-gitlab.univ-grenoble-alpes.fr:MSPL/mspl-2019-2020.git`
   - Check for update and (merge or rebase) them locally by : `git pull mspl-2019-2020 master`
        for other choices have a look at fetch, merge and rebase commands
   - Push then to your gitlab repository: `git push origin master`

   Repeat the last two commands to keep your gitlab repository updated.

   This is necessary to keep yourself up to date with latest TD updates.

Congratulations, you're done for the TD1.


## TD1: Alternatives

You may want to try alternatives to the RStudio notebooks and/or to the R programming language.

1. **Jupyter** is probably the most widely used software for notebooks. It supports many programming languages,
   including R, Python and Julia. Its [official website](https://jupyter.org/) tells you how to install and use it, you
   can even try one of their online demos without installing anything. Note that if you wish to use Jupyter, we strongly
   advise you to install it on your laptop and not rely on the online demo.
2. **Python** is a general-purpose programming language that can be used for doing statistics and data visualization.
   Its [official website](https://www.python.org/) tells you how to install it and provides some basic tutorials.

All the exercises of MSPL can be done with the R or Python programming language in a RStudio or Jupyter notebook. We
advise that you try both languages as they each have their own pros and cons.
