# Groupe
- De Lemos Almeida Pierre
- Dumas Clément

# Lien du jeu de données
Banque Mondiale : http://api.worldbank.org/v2/fr/indicator/NY.GDP.PCAP.CD?downloadformat=csv
Our World in Data : blob:https://ourworldindata.org/80daad88-1494-44ed-add3-555211d29bd3

# Questions proposées
- Y a t il une correlation entre les pays et la taille de ses habitants? 
- Le niveau de richesse (pib) d'un pays influe t il sur la taille de la population ?
- Est ce que l'hisoire influe sur la taille des personnes ?

# Question retenue
Le niveau de richesse (pib) d'un pays influe t il sur la taille de la population ?

# Remarques éventuelles
Le lien du csv des tailles change regulierement (https://ourworldindata.org/human-height#citation, dans la partie "How has height changed globally?", c'est le fichier data)
